'use strict';

var config = require('../config');
var UrlModel = require('../components/url');
var express = require('express');

var router = express.Router();

router.get('/:code', function(req, res, next) {
    var code = req.param('code');
    UrlModel.load(code).then(function(orig) {
        if (orig) {
            res.redirect(301, orig);
        } else {
            var er = new Error('URL Not Found');
            er.status = 404;
            next(er);
        }
    }, function(err) {
        var er = new Error(err);
        er.status = 500;
        next(er);
    });
});

module.exports = router;
