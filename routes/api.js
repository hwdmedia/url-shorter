'use strict';

var config = require('../config');
var UrlModel = require('../components/url');
var express = require('express');
var URL = require('url');

var router = express.Router();

router.post('/', function(req, res) {
    var url = req.body.url;
    if (url) {
        UrlModel.create(url).then(function(code) {
            res.json({
                meta: {
                    code: 200
                },
                data: {
                    orig: url,
                    href: 'http://' + config.shortDomain + '/' + code
                }
            });
        }, function(err) {
            res.status = 500;
            res.json({
                meta: {
                    code: 500,
                    message: err
                }
            });
        });
    } else {
        res.status = 400;
        res.json({
            meta: {
                code: 400,
                message: 'URL is not specified.'
            }
        });
    }
});

router.get('/', function(req, res) {
    var url = req.query.url;
    if (url) {
        var data = URL.parse(url);
        if (data.pathname) {
            var code = data.pathname.replace('/', '');
            UrlModel.load(code).then(function(orig) {
                if (orig) {
                    res.json({
                        meta: {
                            code: 200
                        },
                        data: {
                            orig: orig,
                            href: 'http://' + config.shortDomain + '/' + code
                        }
                    });
                } else {
                    res.status = 404;
                    res.json({
                        meta: {
                            code: 404,
                            message: 'URL is not found.'
                        }
                    });
                }
            }, function(err) {
                res.status = 500;
                res.json({
                    meta: {
                        code: 500,
                        message: err
                    }
                });
            });
        } else {
            res.status = 400;
            res.json({
                meta: {
                    code: 400,
                    message: 'Invalid URL.'
                }
            });
        }
    } else {
        res.status = 400;
        res.json({
            meta: {
                code: 400,
                message: 'URL is not specified.'
            }
        });
    }
});

module.exports = router;
