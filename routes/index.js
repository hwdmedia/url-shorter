'use strict';

var config = require('../config');
var UrlModel = require('../components/url');
var express = require('express');

var router = express.Router();

router.get('/', function(req, res) {
    var url = req.query.url;
    if (url) {
        UrlModel.create(url).then(function(code) {
            res.render('index', {
                apiUrl: req.protocol + '://' + req.get('host') + '/api',
                url: url,
                short: 'http://' + config.shortDomain + '/' + code
            });
        }, function(err) {
            res.render('index', {
                apiUrl: req.protocol + '://' + req.get('host') + '/api',
                url: url,
                message: err
            });
        });
    } else {
        res.render('index', {
            apiUrl: req.protocol + '://' + req.get('host') + '/api'
        });
    }
});

router.get('/docs', function(req, res) {
    res.render('docs', {
        apiUrl: req.protocol + '://' + req.get('host') + '/api',
        shortUrl: 'http://' + config.shortDomain
    });
});

module.exports = router;
