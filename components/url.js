'use strict';

var config = require('../config');
var mongoose = require('mongoose');
var randomstring = require('randomstring');
var Q = require('q');

var UrlModel = mongoose.model('Url', new mongoose.Schema({
    code: { type: String, required: true, unique: true },
    orig: { type: String, required: true, unique: true }
}));

// Generate unique code
var generateCode = function() {
    var code = '';
    do {
        code = randomstring.generate(config.codeLength);
    } while (0 <= config.codeIgnore.indexOf(code));
    return code;
};

module.exports = {

    create: function(orig) {
        // TODO: Handle orig url
        var defer = Q.defer();
        UrlModel.findOne({orig: orig}, function(err, model) {
            if (err) {
                // Error
                defer.reject(err.message);
            } else if (model) {
                // Already exists
                defer.resolve(model.code);
            } else {
                // Create new URL
                model = new UrlModel({
                    code: generateCode(),
                    orig: orig
                });
                model.save(function(err) {
                    if (err) {
                        // Error
                        // TODO: Fix here, try another code
                        defer.reject(err.message);
                    } else {
                        defer.resolve(model.code);
                    }
                });
            }
        });
        return defer.promise;
    },

    load: function(code) {
        var defer = Q.defer();
        UrlModel.findOne({code: code}, function(err, model) {
            if (err) {
                // Error
                defer.reject(err.message);
            } else if (model) {
                // Already exists
                defer.resolve(model.orig);
            } else {
                defer.resolve();
            }
        });
        return defer.promise;
    }

};
