'use strict';

module.exports = {
    // Server port
    port: 3000,
    // Short domain
    shortDomain: '127.0.0.1:3000',
    // Code length of short URL
    codeLength: 6,
    // List of ignored codes
    codeIgnore: [
        'api',
        'docs'
    ],
    // MongoDB connection URL
    mongodb: 'mongodb://localhost/url-shorter'
};
