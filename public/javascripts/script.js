$(function() {
    $('.short-form').submit(function(e) {
        e.preventDefault();
        var shortResult = $('.short-result').addClass('hidden');
        var shortError = $('.short-error').addClass('hidden');
        var apiUrl = $(this).data('api');
        var url = $('input', this).val();
        if (url) {
            $.ajax({
                type: 'POST',
                url: apiUrl,
                dataType: 'json',
                data: {
                    url: url
                },
                success: function(data) {
                    try {
                        shortResult.removeClass('hidden');
                        $('input', shortResult).val(data.data.href)
                    } catch (e) {
                        shortError.html(e).removeClass('hidden');
                    }
                }
            })
        }
    })
});