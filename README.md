# Installation #

* git clone git@bitbucket.org:hwdmedia/url-shorter.git
* cd url-shorter
* npm install
* app.sh start

# Configure #

To configure URL Shorter you need to modify config.js file:

* **port** - Server port
* **shortDomain** - Domain for short links (now it should be site domain)
* **codeLength** - Code length of short URL
* **codeIgnore** - List of ignored codes
* **mongodb** - MongoDB connection URL