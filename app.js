'use strict';

var config = require('./config');
var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var log4js = require('log4js');
var bodyParser = require('body-parser');

var logger = log4js.getLogger();

// Connect to MongoDB
mongoose.connect(config.mongodb);

// Server
var app = express();

app.use(log4js.connectLogger(logger, {level: log4js.levels.INFO}));
app.use(express.static('public'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Routes
app.use('/', require('./routes/index'));
app.use('/api', require('./routes/api'));
app.use('/', require('./routes/redirect'));

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Handling errors
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        status: err.status,
        message: err.message,
        error: app.get('env') === 'development' ? err : {}
    });
});

var server = app.listen(config.port, function() {
    logger.info('Listening on port %d', this.address().port);
});

var shutdown = function() {
    logger.info('Received stop signal, shutting down.');
    if (server) {
        server.close();
    }
};

process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);
