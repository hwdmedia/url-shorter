#!/bin/bash

NAME=app
APP_JS=app.js
SCRIPT=$(readlink -f "$0")
APP_PATH=$(dirname "$SCRIPT")
FOREVER=$APP_PATH/node_modules/forever/bin/forever
PID_FILE=$APP_PATH/$NAME.pid

start() {
    echo "Starting $NAME node instance: "
    exec $FOREVER -a -l $APP_PATH/logs/access.log --pidFile $PID_FILE -e $APP_PATH/logs/errors.log --sourceDir=$APP_PATH start $APP_JS
}

stop() {
    echo -n "Shutting down $NAME node instance: "
    if [ -f $PID_FILE ]; then
        pidId=$(cat $PID_FILE)
        id=$($FOREVER list 2>&1 | awk "{if (\$7 == ${pidId}) { print \$2 } }" | head -n 1 | tr -d '[]')
        exec $FOREVER --sourceDir=$APP_PATH stop $id
    fi
}

case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    *)
        echo "Usage: {start|stop}"
        exit 1
    ;;
esac
